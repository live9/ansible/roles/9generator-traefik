=======
9generator-traefik
=========

A role to bootstrap [traefik](https://traefik.io/) swarm configuration files using [9generator](https://gitlab.com/live9/ansible/playbooks/9generator).

Requirements
------------

* Python 3.
* `htpasswd` command

Role Variables
--------------

This role depends on [9generator-core](https://gitlab.com/live9/ansible/playbooks/9generator-core) role, so it needs the following variables, plus any other needed by that role. This is the list of variables:

#### Mandatory

Global:

* project_type: It always must be _"traefik"_.
* environments: Defines the infrastructure environments where this service is going to be deployed. You need to specify at least one.
* traefik_acme_email: Email for Letsencrypt configuration. Mandatory if `enable_letsencrypt` is True.

Per environment:

* service_hostname: The service hostname for that environment.

#### Optional

Global:
 * version_tag: version tag for the traefik image (default: latest).
 * enable_letsencrypt: Enable Letsencrypt certificate generation. (default: True)
 * insecure_skip_verify: Allow TLS connections to backend containers using self-signed certificates (default: true).
 * enable_resources: Enable container resource limits and reservations. (default: True)

Per environment:

* admin_username: Admin user name. (default: admin)
* replicas: amount of containers to start.
* resources: The container resources to be assigned to the _server_ container (for agent resource please edit the template file itself). Please read swarm official documentation for details on [how resource parameters work](https://docs.docker.com/config/containers/resource_constraints/). Currently we only handle memory resources. If defined, inside this block the following variables can be defined:
    * limits:
        * memory: Maximum memory the container can use. (default: "1024m")
    * reservations:
        * memory: Initial amount of memory assigned to the container. (default: "256m")
   * update_config:
     * parallelism: Maximum amount of containers to update in parallel when doing a [service update](https://docs.docker.com/engine/reference/commandline/service_update/). (default: 2)
     * delay: Delay between container updates. (default: 10s)

Example `manifest.yml` file:

    ---
    project_type: portainer
    version_tag: "latest"
    insecure_skip_verify: False
    traefik_acme_email: "sysadmin@example.com"
    enable_letsencrypt: True
    enable_resources: True

    # Template dependent variables
    environments:
      prod:
        admin_username: administrator
        service_hostname: portainer.example.org
        replicas: 3
        resources:
          limits:
            memory: "1024"
          reservations:
            memory: "256m"
        update_config:
          parallelism: 1
          delay: 5s
      preprod:
        admin_username: staging_admin
        service_hostname: preportainer.example.org
        replicas: 1
        resources:
          limits:
            memory: "512m"
          reservations:
            memory: "256m"
        update_config:
          parallelism: 1
          delay: 5s

##### Setting the admin user password

The admin user password can be set by setting the variable `admin_password` with the plain password. The role will hash it using the `htpasswd` command with the _bcrypt_ algorithm. For security it should be passed vía `ansible-playbook` cli with its value set from and environment variable.

Dependencies
------------

* [9generator-core](https://gitlab.com/live9/ansible/playbooks/9generator-core)

Example Playbook
----------------

    ---
      - name: Bootstrap project
        hosts: localhost
        connection: local
        gather_facts: no
        vars:
          manifest_path: "manifest.yml"
        tasks:
          - include_vars: "{{ manifest_path }}"
          - name: Bootstrap Traefik configuration
            include_role:
              name: 9generator-traefik

License
-------

GPLv3 or later

Author Information
------------------

Juan Luis Baptiste <juan.baptiste _at_ karisma.org.co >
